#!/usr/bin/python
# vim: fileencoding=utf8

import re
#from toolbox import open_sgf 
from os.path import splitext
import sgf_utils
import traceback
from time import time as uxtime
from time import sleep
import IPython


from utils import *
from leela import Leela
from ray import Ray
#from gnugo import GnuGo
from pachi import Pachi

def othercolor(c):
    return (BLACK,WHITE)[c.upper()=="B"]

class Bots:
    def __init__(self,*bots):
        self.bots=bots

    def place(self,*args):
        for bot in self.bots:
            bot.place(*args)

    def undo(self,num=1): 
        for bot in self.bots:
            bot.undo(args,num)

    def showboard(self):
        for bot in self.bots:
            bot.showboard()

    def terminate(self):
        for bot in self.bots:
            bot.terminate()
    
    def komi(self,val):
        for bot in self.bots:
            bot.komi(val)

def wr_long(wr):
    wr = round(wr)
    return "B % 3.0f%%, W % 3.0f%% "%(wr,100-wr)

def mark_deadstones(node,dead_stones=None):
    try:
        try:
            ds=dead_stones.dead_stones()
            dead_stones=ds
        except:
            pass
        for stone in dead_stones:
            node.marker(gtp2sgf(stone))
    except:
        traceback.print_exc()
        #IPython.embed()

def mark_criticalstones(node,critical_stones=None):
    try:
        try:
            ds=critical_stones.critical_stones()
            critical_stones=ds
        except:
            pass
        for stone in critical_stones:
            node.label(gtp2sgf(stone),"C")
    except:
        traceback.print_exc()
        #IPython.embed()

def mark_territory(node,bot):
    try:
        bt = bot.black_territory()
        for stone in bt:
            node.label(gtp2sgf(stone),"B")
        wt = bot.white_territory()
        for stone in wt:
            node.label(gtp2sgf(stone),"W")
    except:
        traceback.print_exc()
        #IPython.embed()

def mark_continuations(node,bot):
    #TODO: das taugt so nicht viel
    try:
        varlist=bot.final_var()
    except:
        varlist = bot
  
    node.comment("Variations for current move")
    letternum=ord("A")
    for pos,wr in varlist:
        node.label(gtp2ij(pos),chr(letternum))
        node.comment("%s: %s"%(chr(letternum),wr_long(wr)))
        letternum+=1
    return

def attack(bot1,bot2,ref,startcolor,nvars=5,varlen=999):
    """ attack or defend possible targets detected by bot1
        ref -- Referee Bot
    """
    bot1.pushboard()
    bot2.pushboard()
    ref.pushboard()
    try:
        title="%s attacks %s"%(bot1.name(),bot2.name())
        print(title)
        if bot1==bot2:
            bot2=None
        targetpoints=bot1.target_points(startcolor)
        variations=[]
        for targetpoint in targetpoints[:nvars]:
            if targetpoint=="PASS":
                continue
            tc = startcolor
            var=[]
            tp = None
            try:
                for i in range(varlen/2):
                    if tp==None:
                        tp = targetpoint
                        bot1.place(tp,tc)
                    else:
                        subtps=bot1.target_points(tc)
                        if len(subtps)==0:
                            if len(var)>varlen/2:
                                break
                            else:
                                subtps=bot1.findlibs(targetpoint)
                                if len(subtps)<2:
                                    break
                                if not title.endswith("aggressive"):
                                    title+=" | aggressive"
                        tp=bot1.play_restricted(tc,subtps)
                        if tp=="PASS":
                            break
                    if bot2!=None:
                        bot2.place(tp,tc)
                    ref.place(tp,tc)
                    var.append((tc,tp)) 
                    tc=othercolor(tc)
                    if bot2!=None:
                        lm=bot2.play(tc)
                        bot1.place(lm,tc)
                    else:
                        subtps=bot1.target_points(tc)
                        if len(subtps)==0:
                            if len(var)>varlen/2:
                                break
                            else:
                                subtps=bot1.findlibs(targetpoint)
                                if len(subtps)<2:
                                    break
                                if not title.endswith("aggressive"):
                                    title+=" | aggressive"
                        lm=bot1.play_restricted(tc,subtps)
                        if lm=="PASS":
                            break
                    ref.place(lm,tc)
                    var.append((tc,lm)) 
                    tc=othercolor(tc)

                if len(var)>0:
                    var_mcs= ref.mc_score()
                    var_wr = ref.winrate()
                    var_dead = ref.dead_stones()
                    if var[-1][0].upper()=="B": 
                        var_wr = 100 - var_wr          
                    final_var=ref.final_var()
                variations.append((var,var_wr,var_mcs,var_dead,title,final_var))
            except:
                traceback.print_exc()
    except:
        traceback.print_exc()
        #IPython.embed()
    finally:
        bot1.popboard()
        if bot2!=None:
            bot2.popboard()
        ref.popboard()
    return variations

def play_bot(bot,bot2,startcolor,varlen):
    bot.pushboard()
    bot2.pushboard()
    try:
        title="%s %s against %s %s"%(bot.name(),startcolor.upper(),bot2.name(),othercolor(startcolor.upper()))
        print(title)
        tc = startcolor
        var=[]
        for i in range(varlen/2):
            rm=bot.play(tc)
            bot2.place(rm,tc)
            var.append((tc,rm)) 
            tc=othercolor(tc)
            lm=bot2.play(tc)
            bot.place(lm,tc)
            var.append((tc,lm)) 
            tc=othercolor(tc)
        if len(var)>0:
            var_mcs=bot2.mc_score()
            var_wr = bot2.winrate()
            var_dead = bot2.dead_stones()
            if var[-1][0].upper()=="B": 
                var_wr = 100 - var_wr          
            final_var=bot2.final_var()
    except:
        traceback.print_exc()
        #IPython.embed()
    finally:
        bot.popboard()
        bot2.popboard()
    return var,var_wr,var_mcs,var_dead,title,final_var

def play_bot_initial(bot,ref,startcolor,varlen):
    bot.pushboard()
    ref.pushboard()
    try:
        c = startcolor
        print("Playing %s initial Choice and %s Continuation"%(bot.name(),ref.name()))
        gtpm=bot.play(c)
        ref.place(gtpm,c)
        luser=ref.playmulti(othercolor(c),num=varlen-1)  
        if len(luser)>0:
            luser.insert(0,(c,gtpm))
            luser_wr = ref.winrate()
            if luser[-1][0].upper()=="B":                 # hier wollen wir die Winrate des ersten Variantenzuges, als für color c
                luser_wr = 100 - luser_wr                   
            luser_mcs = ref.mc_score()
            var_dead = ref.dead_stones()
            final_var = ref.final_var()
    except:
        traceback.print_exc()
        IPython.embed()
    finally:
        bot.popboard()
        ref.popboard()
    return luser,luser_wr,luser_mcs,var_dead,"%s's %s initial choice and %s's continuation"%(bot.name(),c.upper(),ref.name()),final_var

def play_mov_initial(gtpm,ref,startcolor,varlen,name="User"):
    ref.pushboard()
    try:
        c = startcolor
        print("Playing %s initial Choice and %s Continuation"%(name,ref.name()))
        ref.place(gtpm,c)
        luser=ref.playmulti(othercolor(c),num=varlen-1)  
        if len(luser)>0:
            luser.insert(0,(c,gtpm))
            luser_wr = ref.winrate()
            if luser[-1][0].upper()=="B":                 # hier wollen wir die Winrate des ersten Variantenzuges, als für color c
                luser_wr = 100 - luser_wr                   
            luser_mcs = ref.mc_score()
            var_dead = ref.dead_stones()
            final_var = ref.final_var()
    except:
        traceback.print_exc()
        IPython.embed()
    finally:
        ref.popboard()
    return luser,luser_wr,luser_mcs,var_dead,"%s's %s initial choice and %s's continuation"%(name,c.upper(),ref.name()),final_var

def analyze(filename,startnum=2,stopnum=999,varlen=10,playouts=1000,nvariations=3,continue_analysis=False,overwrite=False):
    do_analysis=False
    startnum=max(startnum,2)
    pachi=Pachi()
    pachi.set_time(0,2,1)
    ray=Ray(time_per_move=2.5,tree_size=4096)      # mit --pondering sollte 0.1 Sekunde ausreichend sein
    # ray hängt sich immer noch auf. Trotz des Neustarts bei jedem Undo. Denn müssen wir also mit einem Timeout Thread überwachen. Lohnt den Aufwand nciht.
    #gnugo=GnuGo()
    #gnugo.level(10)    # GNU Go spielt auch als Level 9 noch ziemlich schlecht
    leela=Leela(pondering=False)
    leela.set_time(0,2,1)
    #bots=Bots(leela,gnugo,pachi,ray)
    bots=Bots(leela,pachi,ray)

    game = open_sgf(filename)

    node = game.get_root() 
    gameroot = node
    #gameroot.remove_variations(recursive=True)
    gameroot.remove_illegal_properties(recursive=True)
    if not continue_analysis:       # für das continue brauchen wir die comments
        print("Removing all previous comments")
        #Mi tumlaut kommentaren geht es immer noch nicht
        gameroot.remove_property("C",recursive=True)       # sollte mit dem sgf hack nicht mehr nötig sein

    bots.komi(game.get_komi())
    movenum=0

    def savesgf():
        try: 
            if overwrite:
                name = filename
            else:
                name,ext = splitext(filename)
                name = name+"_an"+ext
            print("saving %s moveno: %i"%(name,movenum))
            write_sgf(name,game)
        except:
            traceback.print_exc()
            error="Fehler beim sgf saven"
            IPython.embed()

    savesgf()

    leela_matchcounter_b=0
    leela_matchcounter_w=0
    pachi_matchcounter_b=0
    pachi_matchcounter_w=0
    #gnugo_matchcounter_b=0
    #gnugo_matchcounter_w=0
    ray_matchcounter_b=0
    ray_matchcounter_w=0

    starttime=uxtime()
    while True:
        if len(node)==0:
            # weiterspielen nach dem Fileende
            c = othercolor(c)
            m = gtp2ij(leela.play(c))
            childnode = node.new_child()
            childnode.move(c,m)
        handicapstones=node.get_setup_stones()
        for color, stones in zip(["B","W"],handicapstones[:2]):
            for pos in stones:
                bots.place(ij2gtp(pos),color)
        node = node[0]   
        try:
            c,m=node.move()
            if c==None:
                continue
            movenum+=1
            print((movenum,uxtime()-starttime))
            if movenum>stopnum:
                break
            c=Color(c)
            print(c,m)
            gtpm=ij2gtp(m)
            if gtpm.upper()=="RESIGN":
                    break
            if gtpm.upper()=="PASS":
                print("PASS") 
                break                   # wenn wir hier weitermachen wollten, mussten wir zum nächsten Node weiterkraxeln
            if continue_analysis and not do_analysis and node.parent.parent!=None:
                ckey = "### Next Move"
                if (ckey in node.parent.parent.comment()) and (not ckey in node.comment()):
                    print("End of former Analysis found at Move #%s"%movenum)
                    do_analysis=True
                else:
                    print("dont analyze this move")
                    do_analysis=False
            else:
                if movenum>=startnum:
                    do_analysis=True
            
            if do_analysis:
                variationlist=[]

                v = play_mov_initial(gtpm,leela,c,varlen,name=node.player())
                if len(v[0])>0:
                    variationlist.append(v)

                print("########### Playing Leelas Choice ###########")
                leela.pushboard()
                lpref,varpos=leela.playmulti(startcolor=c,num=varlen,variations=nvariations)  
                if len(lpref)>0:
                    lc,mv=lpref[-1]
                    lpref_wr = leela.winrate()
                    if lc.upper()=="B":
                        lpref_wr = 100 - lpref_wr
                    lpref_mcs = leela.mc_score()
                    var_dead = leela.dead_stones()
                final_var = leela.final_var()
                variationlist.append((lpref,lpref_wr,lpref_mcs,var_dead,"%s's choice"%leela.name(),final_var))
                leela.popboard()
                if lpref[0][1]==gtpm:
                    if c==BLACK:
                        leela_matchcounter_b+=1
                    elif c==WHITE:
                        leela_matchcounter_w+=1
                    else:
                        print("Color neither Black nor White")
                        IPython.embed()
                """
                v = play_bot(ray,leela,c,varlen)
                if len(v[0])>0:
                    variationlist.append(v)
                """

                print("Playing Leelas Variations")
                for vp in varpos: 
                    leela.pushboard()
                    leela.place(vp,c)
                    var=leela.playmulti(othercolor(c),num=varlen-1,variations=0)
                    var.insert(0,(c,vp))
                    if len(var)>0:
                        vc=var[-1][0]
                        var_mcs=leela.mc_score()
                        var_wr = leela.winrate()
                        var_dead = leela.dead_stones()
                        if vc.upper()=="B":
                            var_wr = 100 - var_wr               #anscheinend stimmt das was mit der winrate polarity nicht, abh. von der Varlen?
                        final_var = leela.final_var()
                        variationlist.append((var,var_wr,var_mcs,var_dead,"%s's Variations"%leela.name(),final_var))
                    leela.popboard() 

                v = play_bot_initial(pachi,leela,c,varlen)
                if len(v[0])>0:
                    variationlist.append(v)
                    if v[0][1]==gtpm:
                        if c==BLACK:
                            pachi_matchcounter_b+=1
                        elif c==WHITE:
                            pachi_machcounter_w+=1
                        else:
                            print("Color neither Black nor White")
                            IPython.embed()

                v = play_bot_initial(ray,leela,c,varlen)
                if len(v[0])>0:
                    variationlist.append(v)
                    if v[0][1]==gtpm:
                        if c==BLACK:
                            ray_matchcounter_b+=1
                        elif c==WHITE:
                            ray_machcounter_w+=1
                        else:
                            print("Color neither Black nor White")
                            IPython.embed()

                """
                v = play_bot_initial(gnugo,leela,c,varlen)
                if len(v[0])>0:
                    variationlist.append(v)
                    if v[0][1]==gtpm:
                        if c==BLACK:
                            gnugo_matchcounter_b+=1
                        elif c==WHITE:
                            gnugo_machcounter_w+=1
                        else:
                            print("Color neither Black nor White")
                            IPython.embed()

                """
                """ 
                v = play_bot(gnugo,leela,c,varlen)
                if len(v[0])>0:
                    variationlist.append(v)
                """

                #variationlist.extend(attack(gnugo,gnugo,ref=leela,startcolor=c,varlen=varlen))

            # playing game move
            bots.place(gtpm,c)
            if do_analysis:
                wr = leela.winrate()
                if c==BLACK:
                    wr = 100 - wr
                node.comment(wr_long(wr),sep="")
                # node.comment("MC Score: %s"%leela.mc_score())
                node.comment("Final Score: %s"%leela.final_score())
            if len(node)==0:
                 break
            if do_analysis:
                dead_stones = leela.dead_stones()
                if len(dead_stones)>0:
                    overviewnode=node.new_child()
                    overviewnode.comment("Overview:\nX -- Dead Stones")
                    mark_deadstones(overviewnode,dead_stones)   
                node.comment("Leela Final Score: %s"%leela.final_score())
                """
                if movenum>220:                             # BUG: hier werden die leeren Nodes mitgezählt
                    mark_territory(overviewnode,gnugo)      # die gnugo calcs kosten viel Zeit
                    overviewnode.comment("GNU Go Final Score: %s"%gnugo.final_score())
                """
                vsnode = node.insert_new_node()
                vsnode.comment("### Next Move for %s ###"%c)
                letter="@"
                for varchain,var_wr,var_mcs,var_dead,var_title,final_var in variationlist:
                    if len(varchain)>1:
                        playername=var_title.split()[0].split("'")[0]
                        variation= vsnode.new_child()
                        vstart = variation
                        for vc,vm in varchain:
                            if vm.upper() == "RESIGN":
                                continue
                            if vm.upper() == "PASS":
                                continue
                            variation.move(vc,gtp2ij(vm))
                            variation=variation.new_child()
                        # deleting last (empty) child
                        empty_child=variation
                        variation=empty_child.parent
                        empty_child.delete()
                        # create variation Overview
                        fignode=sgf_utils.create_figure(vstart,index=vstart.parent.index(vstart))   # notwendig wegen der toten Steine im Overview
                        #mark_deadstones(fignode,var_dead)        # ist blöd hier, weil die Markierungen überschrieben werden
                        mark_deadstones(variation,var_dead)
                        mark_continuations(variation,leela)
                        fignode.comment(var_title)
                        fignode.name(u"%s OV"%var_title.split("'")[0])
                        vstart.name(u"%s Var"%var_title.split("'")[0])
                        varnode = vstart
                        # commenting every varnode
                        while True: 
                            varnode.comment(var_title)
                            if len(varnode)==0:
                                break
                            varnode = varnode[0]
                        varnode.comment(wr_long(var_wr))
                        fignode.comment(wr_long(var_wr))
                        #varnode.comment("MC Score: %s"%var_mcs)            # der taugt nicht viel
                        vstart.comment("%s: %s"%(varchain[0][1],wr_long(var_wr)))
                        if varchain[0][1].upper() not in ("RESIGN","PASS"):
                            labeltxt=vsnode.label(gtp2ij(varchain[0][1]))
                            if labeltxt=="":
                                if letter=="@":
                                    lt = letter
                                    letter = ""
                                else:
                                    lt = playername[0].upper()
                                    if "Variat" in var_title:
                                        lt = lt.lower()
                                labeltxt=lt
                                vsnode.label(gtp2ij(varchain[0][1]),labeltxt)
                            vsnode.comment("%s %s: %s  %s"%(labeltxt,playername,varchain[0][1],wr_long(var_wr)))
                        mark_continuations(variation,final_var) 
                #IPython.embed() 
                if movenum%1==0:
                    savesgf()
        except UnicodeEncodeError:
            raise Exception
        except:
            traceback.print_exc()
            error="analyze error inspector"
            IPython.embed()

    node.endnode().comment("Leela - %s Black Match Counter: %i of %i moves, e.g.  %3.1f%%"%(node.player(BLACK),leela_matchcounter_b,movenum,leela_matchcounter_b *100./movenum))
    node.endnode().comment("Leela - %s White Match Counter: %i of %i moves, e.g.  %3.1f%%"%(node.player(WHITE),leela_matchcounter_w,movenum,leela_matchcounter_w *100./movenum))
    node.endnode().comment("Pachi - %s Black Match Counter: %i of %i moves, e.g.  %3.1f%%"%(node.player(BLACK),pachi_matchcounter_b,movenum,pachi_matchcounter_b *100./movenum))
    node.endnode().comment("Pachi - %s White Match Counter: %i of %i moves, e.g.  %3.1f%%"%(node.player(WHITE),pachi_matchcounter_w,movenum,pachi_matchcounter_w *100./movenum))
    node.endnode().comment("Ray - %s Black Match Counter: %i of %i moves, e.g.  %3.1f%%"%(node.player(BLACK),ray_matchcounter_b,movenum,ray_matchcounter_b *100./movenum))
    node.endnode().comment("Ray - %s White Match Counter: %i of %i moves, e.g.  %3.1f%%"%(node.player(WHITE),ray_matchcounter_w,movenum,ray_matchcounter_w *100./movenum))
    #node.endnode().comment("GNU Go - %s Black Match Counter: %i of %i moves, e.g.  %3.1f%%"%(node.player(BLACK),gnugo_matchcounter_b,movenum,gnugo_matchcounter_b *100./movenum))
    #node.endnode().comment("GNU Go - %s White Match Counter: %i of %i moves, e.g.  %3.1f%%"%(node.player(WHITE),gnugo_matchcounter_w,movenum,gnugo_matchcounter_w *100./movenum))
    savesgf()
    print("fertg")
    bots.terminate()
    #leela.close()


if __name__=="__main__":
    import sys
    startmove=5
    stopmove=999
    playouts=1000
    varlen=16               # varlen muss gerade sein, sonst stimmt die winrate estimation nicht? Weil die Variations alle mit der gleichen Farbe enden sollten und die Bot2Bot Variations sind halt immer evern
    nvariations=1           # mehr bringt nicht viel, verlängert aber die Rechenzeit massiv
    cont=False
    overwrite=False
    args=sys.argv[1:]
    filenames=[]
    for arg in args:
        if "=" in arg:
            var,val=arg.split("=")
            try:
                exec("_="+var)
                exec(arg)
            except:
                traceback.print_exc()
                raise
        else:
            filenames.append(arg)

    if len(filenames)==0:
        import IPython
        l = Leela(playouts=playouts)
        r = Ray(playouts=playouts,time_per_move=time)
        IPython.embed()
    else:
        for filename in filenames:
            analyze(filename,
                    int(startmove),
                    int(stopmove),
                    playouts=int(playouts),
                    varlen=int(varlen),
                    nvariations=int(nvariations),
                    continue_analysis=cont,
                    overwrite=overwrite)

