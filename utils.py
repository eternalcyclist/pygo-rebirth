import traceback
import xsgf as sgf
from gomill import  sgf_moves
import IPython

class Color(object):

    __color__=b"B"

    def __init__(self, color):
        if color==None:
            return None                 # oder sollte man hier einenn Fehler raisen?
        color=bytes(color)
        self.__color__=color[0].upper()

    def __invert__(self):
        if self==b"B":
            return Color(b"W")
        else:
            return Color(b"B")
    # tut noch nicht
    #__neg__==__invert__

    def __eq__(self,c):
        if c==None:
            return False

        if c==1:
            if self.__color__==b"B":
                return True
            else:
                return False
        if c==0:
            if self.__color__==b"W":
                return True
            else:
                return False

        c=bytes(c.upper())
        if c[0]==bytes(self.__color__):
            return True
        else:
            return False 

    def __neg__(self):
        return self.__invert__()

    def __repr__(self):
        return self.__color__.upper()

    def __str__(self):
        if self=="B":
            return "Black"
        else:
            return "White"

    def upper(self):
        return self.__color__.upper()

    def lower(self):
        return self.__color__.lower()

    def other(self):
        return self.__invert__()


BLACK = Color("B")
WHITE = Color("W")
B=BLACK
W=WHITE



def gtp2ij(move,upsidedown=False):
	try:
		letters="ABCDEFGHJKLMNOPQRSTUVWXYZ"
                m = int(move[1:])-1
                if upsidedown:
                    m = 18-m
		return m,letters.index(move[0])
	except:
		raise GRPException("Cannot convert GTP coordinates "+str(move)+" to grid coordinates!")




def ij2gtp(m):
	# (17,0) => a18
	try:
		if m==None:
			return "PASS"
		i,j=m
		letters="ABCDEFGHJKLMNOPQRSTUVWXYZ"
		return letters[j]+str(i+1)
	except:
		raise GRPException("Cannot convert grid coordinates "+str(m)+" to GTP coordinates!")

def sgf2ij(m):
	# cj => 8,2
	a, b=m
	letters="abcdefghjklmnopqrstuvwxyz"
	i=letters.index(b)
	j=letters.index(a)
	return i, j

def sgf2ij_new(m):
	# cj => 8,2
	x, y=m
	letters="abcdefghijklmnopqrstuvwxyz"
	i=letters.index(y)
	j=letters.index(x)
	return 18 -i, j

def ij2sgf(m):
	# (17,0) => ???
	try:
		if m==None:
			return "pass"
		i,j=m
		letters=['a','b','c','d','e','f','g','h','j','k','l','m','n','o','p','q','r','s','t']
		return letters[j]+letters[i]
	except:
		raise GRPException("Cannot convert grid coordinates "+str(m)+" to SGF coordinates!")


def ij2sgf_new(m):
	# (17,0) => ???
	try:
		if m==None:
			return "pass"
		y,x=m
		letters=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t']
		return letters[x]+letters[18-y]
	except:
		raise GRPException("Cannot convert grid coordinates "+str(m)+" to SGF coordinates!")

def gtp2sgf(m):
    x,y=gtp2ij(m)
    return ij2sgf_new((x,y))

def sgf2gtp(m):
    x,y = sgf2ij_new(m)
    return ij2gtp((x,y)) 

def open_sgf(filename):
	try:
		filename2=filename
		if type(filename2)==type(u"abc"):
			if sys.getfilesystemencoding()!="mbcs":
				filename2=filename2.encode(sys.getfilesystemencoding())
		txt = open(filename2,'r')
		content=clean_sgf(txt.read())
		txt.close()
		game = convert_sgf_to_utf(content)
		return game
        except:
            traceback.print_exc()
            raise

def write_sgf(filename,sgf_content):
	try:
		if type(sgf_content)==type("abc"):
			content=sgf_content
		else:
			content=sgf_content.serialise()
		filename2=filename
		if type(filename2)==type(u"abc"):
			if sys.getfilesystemencoding()!="mbcs":
				filename2=filename2.encode(sys.getfilesystemencoding())
		try:
			new_file=open(filename2,'w')
			new_file.write(content)
		except:
			new_file=codecs.open(filename2,"w","utf-8")
			new_file.write(content)

		new_file.close()
	except IOError, e:
		raise GRPException(_("Could not save the RSGF file: ")+filename+"\n"+e.strerror)
	except Exception,e:
		raise GRPException(_("Could not save the SGF file: ")+filename+"\n"+unicode(e))

def convert_sgf_to_utf(content):
	game = sgf.Sgf_game.from_string(content)
	gameroot=game.get_root()
	sgf_moves.indicate_first_player(game) #adding the PL property on the root
        if 0: # node_has(gameroot,"CA"):
		ca=node_get(gameroot,"CA")
                if 0: #ca=="UTF-8":
			#the sgf is already in UTF, so we accept it directly
			return game
		else:
			encoding=(codecs.lookup(ca).name.replace("_", "-").upper().replace("ISO8859", "ISO-8859")) #from gomill code
			content=game.serialise()
                        # TODO: Hier liegt wohl der Fehler mit den Umlauten:
			content=content.decode(encoding,errors='ignore') #transforming content into a unicode object
			content=content.replace("CA["+ca+"]","CA[UTF-8]")
			game = sgf.Sgf_game.from_string(content.encode("utf-8")) #sgf.Sgf_game.from_string requires str object, not unicode
			return game
	else:
            try:
		content=game.serialise()
		#content=content.decode("utf",errors="ignore").encode("utf")
		content=content.encode("utf")
		game = sgf.Sgf_game.from_string(content,override_encoding="UTF-8")
            except:
                traceback.print_exc()
                IPython.embed()
	return game

def clean_sgf(txt):
	#txt is still of type str here....

	#https://github.com/pnprog/goreviewpartner/issues/56
	txt=txt.replace(str(";B[  ])"),str(";B[])")).replace(str(";W[  ])"),str(";W[])"))

	#https://github.com/pnprog/goreviewpartner/issues/71
	txt=txt.replace(str("KM[]"),str(""))
	txt=txt.replace(str("B[**];"),str("B[];")).replace(str("W[**];"),str("W[];"))

	return txt

