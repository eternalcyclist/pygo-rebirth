#!/usr/bin/python
# vim: fileencoding=utf8
""" 
execute analyzer commands embedded in game comments
"""

from toolbox import open_sgf
from leela import Leela
from ray import Ray
from gnugo import GnuGo
from pachi import Pachi


def init_leela():
    leela = Leela()
    return leela

def init_ray():
    ray = Ray()
    return ray

def init_gnugo():
    gnugo = GnuGo()
    gnugo.level(10)
    return gnugo

def init_pachi():
    pachi = Pachi()
    return pachi

bots = {
        "leela":init_leela,
        "ray":init_ray,
        "pachi":init_pachi,
        "gnugo":init_gnugo,
        }

def play(node,args):
    """
    args:
    leela - use leela
    pachi - use pachi
    ray   - use ray
    gnugo - use gnugo
    black - play first move as black
    white - play first move as white
    [A..T][1..19] - play first move at pos
    end - play until end 
    <num> - play num moves
    """
    
    bot = None
    for arg in args:
        if arg in bots:
            bot = bots[arg]
            args.remove(arg)
            break
    if bot==None:
        bot = init_leela()
    bot.placemulti(node.move_history())    




commands = { "play":play
        }

def search_and_execute(node):
    for line in node.comment():
        if line.startswith("!"):
            line = line.lower()
            words=line.split()
            command = words[0][1:]
            if command in commands:
                commands[command](node,words)    


def analyze(filename):
    game = open_sgf(filename)
    node = game.get_root() 
    search_and_execute(node)
    


if __name__=="__main__":
    import sys
    filename = sys.argv[1]
    analyze(filename)



