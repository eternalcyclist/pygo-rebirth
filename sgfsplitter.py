#!/usr/bin/python

from __future__ import unicode_literals

from toolbox import *
from gomill import sgf_properties
import sgf_utils
import traceback
import os.path
import IPython


def split(filename,moves=None):
    """ split the game at given move numbers.
    If len(moves) ==1 and moves< 10, the treat the value as number of equal pieces 
    the game will be split into.
    We actually don't split the game tree, but just remove the variations.
    """
    game = open_sgf(filename)
    #game.set_charset("UTF-8")
    root = game.get_root()
    nmoves = game.total_movenum()
    if len(moves)==1:
        moves=range(nmoves/moves[0])
   NOCH NICHT READY 
    IPython.embed()
    name,ext =os.path.splitext(filename)
    write_sgf(name +"_clean"+ext,game)

if __name__ == "__main__":
    from sys import argv
	
    split(sys.argv[1],list(map(int,sys.argv[2:])))




