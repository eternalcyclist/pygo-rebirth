#!/usr/bin/python
# vim: fileencoding=utf8

from gtp import gtp
import re
from utils import  *
from os.path import splitext
from gomill import sgf_utils
import traceback
from time import time as uxtime
from time import sleep

class Pachi(gtp):

    def __init__(self):
        #gtp.__init__(self,["../Pachi/pachi-12.45-amd64 -d 0 --nodcnn pondering"],use_stderr=False,use_stderr_queue=False)
        gtp.__init__(self,["../Pachi/pachi-12.45-amd64 -d 0 --nopatterns pondering"],use_stderr=False,use_stderr_queue=False)

    def predict(self,color,pos):
        self.write("predict %s %s"%(color,pos))
        return self.readlines()


"""
genmove-analyze  gibt live winrates währnd der Zugberecnung aus

predcit color move    so eine Art Quiz, welches der beste Zug ist

lz-analyze   liefert live stats über stderr

pachi-score_est

final_status_list alive, seki, dead, territory

pachi-evaluate color -> alle board positions als liste mit values -- vermutlich winrate 

pachi-result     -> color move plyouts winrate dynkomi   --- tut wohl nicht

pachi-genmoves   -> segfault

gogui-joseki--moves    geht nicht, weil keine JosekiDB ladbar




"""



if __name__=="__main__":
    import sys
    startmove=4
    stopmove=999
    playouts=10000
    varlen=10
    nvariations=5
    time=1
    args=sys.argv[1:]
    filenames=[]
    for arg in args:
        if "=" in arg:
            var,val=arg.split("=")
            try:
                exec("_="+var)
                exec(arg)
            except:
                traceback.print_exc()
        else:
            filenames.append(arg)

    if len(filenames)==0:
        import IPython
        p = Pachi()  
        IPython.embed()

