#!/usr/bin/python
# vim: fileencoding=utf8

from gtp import gtp
import re
from utils import  *
from os.path import splitext
from gomill import sgf_utils
import traceback
from time import time as uxtime
from time import sleep

def othercolor(c):
    return ("B","W")[c.upper()=="B"]

class GnuGo(gtp):

    _level=9

    def __init__(self):
        gtp.__init__(self,["gnugo --mode gtp"])

    def level(self,num=None):
        if num!=None:
            if self.writec("level %s"%num):
                self._level=num
        return self._level

    def load_sgf(self,filename,moveno=""):
        return self.writec("loadsgf %s %s"%(filename,moveno))

    def last_move(self):
        color,pos = self.query("last_move").split()
        return(pos,color[0].upper())

    def play_restricted(self,color,positions):
        if color.upper() in ("B","BLACK"):
            color="black"
        else:
            color="white"
        postext=""
        for p in positions:
            if p.upper() in ("PASS","RESIGN"):
                continue
            else:
                postext += p + " "
        if postext=="":
            return "PASS"
        suggested_move= self.query("restricted_genmove %s "%color + postext)
        if suggested_move.upper()=="PASS":
            return "PASS"
        self.place(suggested_move,color)
        return suggested_move

    def findlibs(self,pos):
        if self.color_at(pos)=="E":
            return []
        return self.query("findlib "+pos).split()

    def attack(self,pos,owl=False):
        if owl:
            cmd = "owl_attack"
        else:
            cmd = "attack"
        ans=self.query("%s %s"%(cmd,pos)).split()
        code=int(ans[0])
        if len(ans)==1:
            pos=None
        else:
            pos=ans[1]
        return code,pos

    def defend(self,pos,owl=False):
        if owl:
            cmd = "defend"
        else:
            cmd = "owl_defend"
        ans=self.query("%s %s"%(cmd,pos)).split()
        code=int(ans[0])
        if len(ans)==1:
            pos=None
        else:
            pos=ans[1]
        return code,pos

    def color_at(self,pos):
        return self.query("color %s"%pos)[0].upper()

    def dragon_status(self,status=None,color=None,pos=""):
        if color!=None:
            bdragons,wdragons=self.dragon_status(status=status,pos=pos,color=None)
            if color.upper()=="B":
                return bdragons
            else:
                return wdragons
        if pos!="":
            ans = self.query("dragon_status " + pos)
            words = ans.split()
            return [pos].extend(words)
        else:
            ans = self.mquery("dragon_status")
            wdragons=[]
            bdragons=[]
            for line in ans:
                words=line.split()
                if status!=None:
                    if status.lower()!=words[1]:
                        continue
                words[0]=words[0][:-1]
                if self.color_at(words[0])=="B":
                    bdragons.append(words)
                elif self.color_at(words[0])=="W":
                    wdragons.append(words)
                else:
                    print("Wrong Color")
                    IPython.embed()
        return bdragons,wdragons

    def target_points(self,color=None):
        """ Get target points for color move, e.g. for attacking othercolors groups
        """
        ds = self.dragon_status("critical",color=color)
        if color==None:
            dtemp=ds[:]
            dtemp[0].extend(dtemp[1])
            ds=dtemp[0]
        tpoints=[]
        for d in ds:
            tpoints.extend(d[2:])
        return list(set(tpoints))

    def dragon_data(self,pos=""):
        # slow!
        ans = self.mquery("dragon_data "+pos)
        wdragons=[]
        bdragons=[]
        dragon=None
        for line in ans:
            if ":" in line:
                if dragon!=None:
                    if dragon["color"]=="black":
                        bdragons.append(dragon)
                    else:
                        wdragons.append(dragon)
                dragon={}
                continue
            key,val=line.split(" ",1)
            dragon[key]=val.strip()
        return bdragons,wdragons

    def worm_data(self,pos=""):
        # slow!
        ans = self.mquery("worm_data "+pos)
        wworms=[]
        bworms=[]
        eworms=[]
        worm=None
        for line in ans:
            if ":" in line:
                if worm!=None:
                    if worm["color"]=="black":
                        bworms.append(worm)
                    elif worm["color"]=="white":
                        wworms.append(worm)
                    elif worm["color"]=="empty":
                        eworms.append(worm)
                    else:
                        print("wrong worm color")
                        IPython.embed()
                worm={}
                continue
            key,val=line.split(" ",1)
            worm[key]=val.strip()
        return bworms,wworms,eworms

    def attack_and_defend(self,pos,num=None,color=None,owl=False):
        """Attack and defend group at pos until it is done or save or <num> is reached
        """
        if color==None:
            color = othercolor(self.color_at(pos))
        movecnt=0
        movelist=[]
        while True:
            if num!=None:
                if movecnt>num:
                    break
            if color!=self.color_at(pos):
                if self.color_at(pos)=="E":
                    break
                code,p = self.attack(pos)
                print(code,p)
                if not code:
                    break
                self.place(p,color)
                movelist.append((color,p))
                movecnt+=1
                color=othercolor(color)
            if self.color_at(pos)=="E":
                break
            code,p=self.defend(pos)
            print(code,p)
            if not code:
                break
            self.place(p,color)
            movecnt+=1
            color=othercolor(color)
        return movelist

    def black_territory(self):
        return self.final_status_list("black_territory")[0]

    def white_territory(self):
        return self.final_status_list("white_territory")[0]

"""
attack

defend

owl_attack

owl_defend

analyze_semeai

dragon_status

combination_attack

level

undo

movehistory

analyze_eyegraph
"""

if __name__=="__main__":
    import sys
    startmove=4
    stopmove=999
    playouts=None
    varlen=10
    nvariations=5
    args=sys.argv[1:]
    filenames=[]
    for arg in args:
        if "=" in arg:
            var,val=arg.split("=")
            try:
                exec("_="+var)
                exec(arg)
            except:
                traceback.print_exc()
        else:
            filenames.append(arg)

    if len(filenames)==0:
        import IPython
        g = GnuGo()
        gnugo=g
        bot=g
        IPython.embed()

