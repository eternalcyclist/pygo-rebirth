#!/usr/bin/python
# vim: fileencoding=utf8
from gomill.sgf import *
from gomill import sgf

class Node(sgf.Node):
    """
    """

    def last_node(self):
        """ return last node of this variation
        """
        node = self
        while len(node)>0:
            node = node[0]
        return node
    lastnode = last_node


class Tree_node(Node):
    """
    """

class _Root_tree_node(Tree_node):
    """Variant of Tree_node used for a game root."""
    def __init__(self, property_map, owner):
        self.owner = owner
        self.parent = None
        self._children = []
        Node.__init__(self, property_map, owner.presenter)


class Sgf_game(sgf.Sgf_game):
    """
    """

