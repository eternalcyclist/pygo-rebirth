#!/usr/bin/python
# vim: fileencoding=utf8

from gtp import gtp
import re
from utils import  *
from os.path import splitext
from gomill import sgf_utils
import traceback
from time import time as uxtime
from time import sleep

class Ray(gtp):

    def __init__(self,playouts=999999,time_per_move=5,tree_size=None):
        tstr ="--const-time %s"%time_per_move
        pstr ="--playout %s"%playouts
        if tree_size!=None:
            trstr ="--tree-size %s"%tree_size
        else:
            trstr=""
        gtp.__init__(self,["../Ray/ray --pondering",pstr,tstr,trstr],use_stderr=False,use_stderr_queue=False)

    def reset(self):
        self.restart()
        
    
if __name__=="__main__":
    import sys
    startmove=4
    stopmove=999
    playouts=10000
    varlen=10
    nvariations=5
    time=1
    args=sys.argv[1:]
    filenames=[]
    for arg in args:
        if "=" in arg:
            var,val=arg.split("=")
            try:
                exec("_="+var)
                exec(arg)
            except:
                traceback.print_exc()
        else:
            filenames.append(arg)

    if len(filenames)==0:
        import IPython
        r = Ray(playouts=playouts,time_per_move=time)  
        IPython.embed()
    else:
        for filename in filenames:
            analyze(filename,startmove,stopmove,playouts=playouts,varlen=varlen,nvariations=nvariations)

