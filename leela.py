#!/usr/bin/python
# vim: fileencoding=utf8

from gtp import gtp
import re
from os.path import splitext
import traceback
from time import time as uxtime
from time import sleep

class Leela(gtp):

    _varlist=[]
    _var_wr=[]

    def __init__(self,playouts=None,pondering=True):
        if playouts != None:
            pstr = "-p%s"%playouts
        else:
            pstr=""
        if pondering:
            ponder=""
        else:
            ponder="--noponder"
        gtp.__init__(self,["leela_gtp","-g","--nobook",pstr,ponder]) #,use_err_queue=False)

    def winrate(self):
        """ winrate for the player who is next
        """
        return self.fquery("winrate") *100

    def mc_winrate(self):
        return self.fquery("mc_winrate") *100
   
    def vn_winrate(self):
        return self.fquery("vn_winrate") *100

    def mc_score(self):
        ans = self.query("mc_score")     
        return ans

    def load_sgf(self,filename,moveno=""):
        return self.writec("loadsgf %s %s"%(filename,moveno))
    
    def print_sgf(self):
        self.write("printsgf")
        txt =""
        while True:
            line = self.readline_raw()
            txt += line
            if line.endswith(")"):
                for i in range(3):
                    self.readline_raw()
                break
        return txt

    def move_number(self):
        return len(self.move_history())

    def playmulti(self,startcolor=None,num=1,variations=0):
        movelist=[]
        varpos=[]
        if startcolor==None:
            startcolor = self.color_to_play()
        startcolor=startcolor.upper()
        for movenum in range(num):
            c=startcolor
            if startcolor=="B":
                move = self.play_black()
                startcolor="W"
            else:
                move =self.play_white()
                startcolor="B"
            if movenum==0 and variations>0:
                varpos=self.get_varpos()[1:]
            movelist.append((c,move))
            if move.upper() in ("RESIGN","PASS"):
                break
        if variations==0:
            return movelist
        else:
            vlen = min(variations,len(varpos))
            return movelist,varpos[:vlen]
    
    def get_varpos(self):
        poslist=[]
        for var in self._varlist:
            poslist.append(var[0])
        return poslist

    def get_var_wr(self):
        """ winrate for Black in varpos"""
        return self._var_wr

    def final_var(self):
        return zip(self.get_varpos(),self.get_var_wr())

    def _update_varlist(self,color):
        sleep(0.01)
        while not self.stderr_queue.empty():
            line = self.stderr_queue.get().strip()
            #line = self.process.stderr.readline().strip()
            if "->" in line:
                words=line.split()
                self._varlist.append(words[13:])
                wr=float(words[4][:-2])
                if color=="W":
                    wr = 100 -wr 
                self._var_wr.append(wr)
            if "====" in line:
                break
            #sleep(.9)                              # sollte man auf 0 setzen könneen, weil genmove wartet, bis eine Antwort kommt und dann sind diese Results auch shcon da
        while not self.stderr_queue.empty():
            self.stderr_queue.get()

    def place(self,move,color):
        self._varlist=[]
        self._var_wr=[]
        return gtp.place(self,move,color)

    def play_black(self):
        while not self.stderr_queue.empty():
            self.stderr_queue.get()
        self._varlist=[]
        self._var_wr=[]
        retval = gtp.play_black(self)
        self._update_varlist("B")
        return retval

    def play_white(self):
        while not self.stderr_queue.empty():
            self.stderr_queue.get()
        self._varlist=[]
        self._var_wr=[]
        retval = gtp.play_white(self)
        self._update_varlist("W")
        return retval

    """
    def goto(self,move_number):
        mh = self.move_history()
        self.reset()
        for pos,c in mh[:move_number]:
            if pos=="PASS":
                continue
            if c=="AB": c="B"
            if c=="AW": c="W"
            if c=="AE": continue        # TODO
            self.place(pos,c)
        #self.undo(self.move_number()-move_number)

    def undo(self,num=1):
        print("Undoing %s moves"%num)
        if num>0:
            self.goto(-1*num)
    """

    def showboard(self):
        #self.query("showboard")
        #print("no showboard for Leela due to hangup")
        while not self.stderr_queue.empty():
            self.stderr_queue.get()
        self.query("showboard")
        sleep(0.01)
        lines=[]
        while not self.stderr_queue.empty():
            line = self.stderr_queue.get().strip()
            lines.append(line)
            #line = self.process.stderr.readline().strip()
            if "Hash" in line:
                break
            #sleep(.9)                              # sollte man auf 0 setzen könneen, weil genmove wartet, bis eine Antwort kommt und dann sind diese Results auch shcon da
        while not self.stderr_queue.empty():
            self.stderr_queue.get()
        return "\n".join(lines)

if __name__=="__main__":
    import sys
    startmove=4
    stopmove=999
    playouts=None
    varlen=10
    nvariations=5
    args=sys.argv[1:]
    filenames=[]
    for arg in args:
        if "=" in arg:
            var,val=arg.split("=")
            try:
                exec("_="+var)
                exec(arg)
            except:
                traceback.print_exc()
        else:
            filenames.append(arg)

    if len(filenames)==0:
        import IPython
        l = Leela(playouts=playouts)
        IPython.embed()
