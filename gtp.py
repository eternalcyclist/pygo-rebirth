# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import subprocess,sys
import threading, Queue
from time import sleep
from time import time as uxtime
import traceback
import IPython
from utils import ij2gtp

class gtp():

        _name="gtp"
        komi_value=0
        boardstack=[]

        def __init__(self,command,use_stderr=True,use_stderr_queue=True,use_stdout_queue=False):
		self.c=1
		#command=[c.encode(sys.getfilesystemencoding()) for c in command]
		self.command_line=" ".join(command)
                print(self.command_line)
                self.use_stderr_queue=use_stderr_queue
                self.use_stderr=use_stderr
                self.use_stdout_queue=use_stdout_queue
                self.restart()
                #müssen wir das Nachfolgende auch neu starten?
		self.size=0
                if use_stderr_queue:
                    self.stderr_queue=Queue.Queue()
                    threading.Thread(target=self.consume_stderr).start()
                if use_stdout_queue:
                    self.stdout_queue=Queue.Queue()
                    threading.Thread(target=self.consume_stdout).start()
		self.free_handicap_stones=[]
		self.history=[]
                self._name = self.name()

        def __del__(self):
            self.terminate()

        def restart(self):
		try: self.process.kill()
		except: pass
                if self.use_stderr or self.use_stderr_queue:
		    self.process=subprocess.Popen(self.command_line, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE,shell=True)
                else:
                    self.use_stderr_queue=False
		    self.process=subprocess.Popen(self.command_line, stdin=subprocess.PIPE, stdout=subprocess.PIPE,shell=True)

	####low level function####
	def consume_stderr(self):
		while 1:
			try:
				err_line=self.process.stderr.readline().decode("utf-8")
				if err_line:
					self.stderr_queue.put(err_line)
				else:
					print("exiting consume_stderr thread of "+self._name)
					return
			except:
                                traceback.print_exc()
				print("leaving consume_stderr thread due to exception")
				return
	
	def consume_stdout(self):
		while 1:
			try:
				line=self.process.stdout.readline().decode("utf-8")
				if line:
					self.stdout_queue.put(line)
				else:
					print("leaving consume_stdout thread")
					return
			except:
                                traceback.print_exc()
				print("leaving consume_stdout thread due to exception")
				return
	
	def quick_evaluation(self,color):
		return "Feature not implemented"
	
	def write(self,txt):
                print("%s %s gtp.write: %s"%(uxtime(),self._name,txt))
		try:
			self.process.stdin.write(txt+"\n")
		except Exception:
                        traceback.print_exc()
			print("Error while writting to stdin\n")
		#self.process.stdin.write(str(self.c)+" "+txt+"\n")
		self.c+=1

        def writec(self,txt):
            self.write(txt)
            if self.readlinet()[0]=="=":
                return True
            else:
                return False

        def flush(self):
            self.process.stdout.flush()

	def readline(self):
		answer=self.process.stdout.readline().decode("utf-8")
		while answer in ("\n","\r\n","\r"):
			answer=self.process.stdout.readline().decode("utf-8")
		return answer

        def readline_raw(self):
	        return self.process.stdout.readline().decode("utf-8").strip()

        def readlinet(self):
            return self.readlines()[-1]

        def readlines(self):
            lines=[]
            while True:
                line = self.readline_raw()
                if len(line)==0:
                    break
                lines.append(line)
            return lines

        def query(self,txt):
            self.write(txt)
            ans = self.readlinet()
            if len(ans)==0:
                return "" 
            if ans[0]=="=":
                return ans[1:].strip()
            else:
		raise Exception("Error in query()\nquery='"+txt+"'answer='"+ans+"'\n")

        def mquery(self,txt):
            self.write(txt)
            ans = self.readlines()
            if ans[0][0]=="=":
                ans[0]=ans[0][1:].strip()
            return ans

        def fquery(self,txt):
            ans = self.query(txt)
            try:
                ans = float(ans)
            except ValueError:
                ans = 1e99 
            return ans

	####hight level function####
	def boardsize(self,size=19):
		self.size=size
		return self.writec("boardsize "+str(size))
		
	def reset(self):
		return self.writec("clear_board")

	def komi(self,k):
            if self.writec("komi "+str(k)):
			self.komi_value=k
			return True
	    else:
			self.komi_value=0
			return False

	def place_black(self,move):
            return self.place(move,"b")
	
	def place_white(self,move):
            return self.place(move,"w")

        def place(self,move,color):
                if color not in  (1,"B","b","black","BLACK",0,"W","w","white","WHITE"):
                    color,move = move,color 
                if color in (1,"B","b","black","BLACK"):
                    color="b"
                    cstr="black"
                elif color in (0,"W","w","white","WHITE"):
                    color="w"
                    cstr="white"
                else:
                    error="gpt.place: invalid color"
                    IPython.embed()

		if move == "RESIGN":
			print("WARNING: trying to play RESIGN as GTP move")
			self.history.append([color,move])
			return True

                if len(move)==2 and not move[1].isdigit():
                    move = ij2gtp(move)

                if self.writec("play "+cstr+" "+move):
		    self.history.append([color,move])
		    return True
		else:
                    return False
    
        def place_multi(self,movelist):
            for c,pos in movelist:
                self.place(pos,c)

	def play_black(self):
		answer = self.query("genmove black")
		try:
			move=answer.upper()
			self.history.append(["b",move])
			return move
		except Exception:
			raise Exception("Error in genmove_black()\nanswer='"+answer+"'\n")

		
	def play_white(self):
		answer=self.query("genmove white")
		try:
			move=answer.upper()
			self.history.append(["w",move])
			return move
		except Exception:
                        traceback.print_exc()
			raise Exception("Exception in play_white()()\nanswer='"+answer+"'\n")

        def play(self,color):
            if color in (1,"B","b"):
                return self.play_black()
            else:
                return self.play_white()

        def pushboard(self):
            self.boardstack.append(self.history[:])

        def popboard(self,index=-1):
            history = self.boardstack.pop(index)[:]
            self.history=[]
            self.replay(history)

	def replay(self,history):
            self.reset()
            self.komi(self.komi_value)
            try:
                    #adding handicap stones
                    if len(self.free_handicap_stones)>0:
                            self.set_free_handicap(self.free_handicap_stones)
                    for color,move in history:
                            if color.upper()=="B":
                                    if not self.place_black(move):
                                            return False
                            else:
                                    if not self.place_white(move):
                                            return False
                    return True			
            except:
                traceback.print_exc()
                IPython.embed()


	def undo(self,num=1):
            history=self.history[:-num]
            self.history=[]
            return self.replay(history)

        def last_move(self):
            if len(self.history)==0:
                return(("@@","W"))                  # dummy,color to play zu befriedigen
            else:
                return self.history[-1]
                
        def color_to_play(self):
            if self.last_move()[1].upper()=="B":
                return "W"
            else:
                return "B"
            
	def name(self):
		return self.query("name")
	
	def version(self):
		return self.query("version")

	def set_free_handicap(self,positions):
		self.free_handicap_stones=positions[:]
		stones=""
		for p in positions:
			stones+=p+" "
                if self.query("set_free_handicap "+stones.strip()):
			if answer[0]=="=":
				return True
			else:
				return False	
	
	def undo_standard(self):
		return self.query("undo")
	
	def countlib(self,move):
		return self.query("countlib "+move)
	
	def final_score(self):
		return self.query("final_score")
	
	def final_status(self,move):
		return self.query("final_status "+move)

        def final_status_list(self,state):
            groups=[]
            self.write("final_status_list "+state)
            #self.write("final_score")                   # just as terminator for the unterminated answer of final_status_list
            lineno=0
            while True:
                line = self.readline_raw()
                if len(line)==0:
                    break
                if line[0]=="=":
                    if lineno==0:
                        line=line[1:]
                    else:
                        break
                lineno+=1
                line=line.strip()
                words = line.split()
                groups.append(words)
            return groups

        def alive_groups(self):
            return self.final_status_list("alive")

        def alive_stones(self):
            stones=[]
            groups = self.alive_groups()
            for group in groups:
                stones.extend(group)
            return stones

        def dead_groups(self):
            return self.final_status_list("dead")

        def dead_stones(self):
            stones=[]
            groups = self.dead_groups()
            for group in groups:
                stones.extend(group)
            return stones

	def set_time(self,main_time=30,byo_yomi_time=30,byo_yomi_stones=1):
		return self.query("time_settings "+str(main_time)+" "+str(byo_yomi_time)+" "+str(byo_yomi_stones))

        def showboard(self):
            # does not work with leela
            self.write("showboard")
            lines = self.readlines()
            return "\n".join(lines)

	def quit(self):
            self.write("quit")

	def terminate(self):
		t=10
		try: self.process.stdin.close()
		except: pass
		try: self.process.kill()
		except: pass
                """
		while 1:
			#self.quitting_thread.join(0.0)	
			if not self.quitting_thread.is_alive():
				print("The bot has quitted properly")
				break
			elif t==0:
				print("The bot is still running...")
				print("Forcefully closing it now!")
				break
			t-=1
			print("Waiting for the bot to close",t,"s")
			sleep(1)
                """
		
		
	def close(self):
		print("Now closing")
                try:
		    self.quitting_thread=threading.Thread(target=self.quit)
		    self.quitting_thread.start()
		    threading.Thread(target=self.terminate).start()
                except:
                    pass
