#!/usr/bin/python
# vim: fileencoding=utf8

from types import *


def create_figure(node,nmoves=999,annotated_moves=None,commented_moves=[],index=None):
    """ create game figure from the following nmoves of given node as parents variation.
    """

    fig = node.parent.new_child(index)
    cnode = node
    firstnode=cnode
    for moveno in range(1,nmoves+1):
        color,pos = cnode.move()
        if color!=None:
            if pos==None: 
                break
            if type(pos)==StringType:
                if pos.upper() in ("PASS","RESIGN"):
                    print("===== " + pos)
                    break
            fig.stone(pos,color)
            if annotated_moves==None:
                fig.label(pos,"%i"%moveno)
            firstnode.stone(pos,"E")
        if len(cnode)==0:
            break
        cnode = cnode[0]
    return fig


if __name__=="__main__":
    1

